all: toolbox ansible-shell ;

HELM := 3.13.1
KUBECTL := 1.28.2
DOCTL := 1.120.2
KUBENS := 0.9.5
DOCKER := 24.0.6
ANSIBLE := 9.2.0
KUBIE := 0.22.0
KUBEFWD := 1.22.5
YTT := 0.46.0
KUSTOMIZE := 5.1.1
ANSIBLE_OPERATOR := 1.34.0

kaniko:
	cp $(DOCKERFILE) Dockerfile
	kaniko \
		--build-arg HELM_VERSION=$(HELM) \
		--build-arg KUBECTL_VERSION=$(KUBECTL) \
		--build-arg DOCTL_VERSION=$(DOCTL) \
		--build-arg KUBENS_VERSION=$(KUBENS) \
		--build-arg DOCKER_VERSION=$(DOCKER) \
		--build-arg ANSIBLE_VERSION=$(ANSIBLE) \
		--build-arg KUBIE_VERSION=$(KUBIE) \
		--build-arg KUBEFWD_VERSION=$(KUBEFWD) \
		--build-arg KUSTOMIZE_VERSION=$(KUSTOMIZE) \
		--build-arg YTT_VERSION=$(YTT) \
		--build-arg ANSIBLE_OPERATOR=$(ANSIBLE_OPERATOR) \
		--label org.opencontainers.image.title="$(TITLE)" \
		--label org.opencontainers.image.created="$(shell date  --rfc-3339=seconds)" \
		--label org.opencontainers.image.vcs-url="$(shell git remote get-url origin)" \
		--label org.opencontainers.image.revision="$(shell git rev-parse HEAD)" \
		--destination $(IMAGE) \

genericdocker:
	docker build \
		--build-arg HELM_VERSION=$(HELM) \
		--build-arg KUBECTL_VERSION=$(KUBECTL) \
		--build-arg DOCTL_VERSION=$(DOCTL) \
		--build-arg KUBENS_VERSION=$(KUBENS) \
		--build-arg DOCKER_VERSION=$(DOCKER) \
		--build-arg ANSIBLE_VERSION=$(ANSIBLE) \
		--build-arg KUBIE_VERSION=$(KUBIE) \
		--build-arg KUBEFWD_VERSION=$(KUBEFWD) \
		--build-arg KUSTOMIZE_VERSION=$(KUSTOMIZE) \
		--build-arg YTT_VERSION=$(YTT) \
		--build-arg ANSIBLE_OPERATOR=$(ANSIBLE_OPERATOR) \
		--label org.opencontainers.image.title="$(TITLE)" \
		--label org.opencontainers.image.created="$(shell date  --rfc-3339=seconds)" \
		--label org.opencontainers.image.vcs-url="$(shell git remote get-url origin)" \
		--label org.opencontainers.image.revision="$(shell git rev-parse HEAD)" \
		-f $(DOCKERFILE) \
		-t $(IMAGE) \
		.

toolbox:
	$(MAKE) genericdocker TITLE="Toolbox Image" DOCKERFILE=Dockerfile.toolbox IMAGE=registry.gitlab.com/ulrichschreiner/toolbox

ansible-shell:
	$(MAKE) genericdocker TITLE="Ansible Interactive Shell Image" DOCKERFILE=Dockerfile.ansibleshell IMAGE=registry.gitlab.com/ulrichschreiner/toolbox/ansible-shell
